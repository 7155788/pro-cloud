/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.beans.dto;

import com.cloud.admin.beans.po.SysMenu;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author Aijm
 * @Description 菜单详细信息
 * @Date 2019/8/13
 */
@Data
@Accessors(chain = true)
@ApiModel(value="Menu对象", description="菜单")
public class MenuDTO extends SysMenu {

    /**
     * 菜单类型  1：表示显示 0： 表示隐藏菜单
     */
    public static final Integer HAS_SHOW = 1;

    public static final Integer HAS_HIDE = 0;


    /**
     * 目录类型
     */
    public static final Integer TYPE_CATALOG = 0;
    /**
     * 组建
     */
    public static final Integer TYPE_COMPONENT = 1;
    /**
     * 外部链接
     */
    public static final Integer TYPE_OUT_LINK= 2;
    /**
     * 按钮
     */
    public static final Integer TYPE_BUTTON = 3;


}