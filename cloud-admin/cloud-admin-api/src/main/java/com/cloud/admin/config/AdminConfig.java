/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.config;

import com.cloud.admin.componse.PermissionServiceImpl;
import com.cloud.common.security.component.IPermissionService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author Aijm
 * @Description admin 暴露模块出去的配置
 * @Date 2020/5/9
 */
@Configuration
public class AdminConfig {

    /**
     * 配置默认的权限配置
     * @return
     */
    @Bean(name = "pms")
    @ConditionalOnMissingBean(IPermissionService.class)
    public IPermissionService pms() {
        return new PermissionServiceImpl();
    }
}