/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.oauth.properties;


import lombok.Data;

/**
 * @Author Aijm
 * @Description 验证码配置
 * @Date 2019/5/20
 */
@Data
public class ValidateCodeProps {

	/**
	 * 图片验证码配置
	 */
	private ImageCodeProps image = new ImageCodeProps();
	/**
	 * 短信验证码配置
	 */
	private SmsCodeProps sms = new SmsCodeProps();
	/**
	 * 邮箱验证码配置
	 */
	private EmailCodeProps email = new EmailCodeProps();

}