/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.util;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.cloud.common.data.util.props.SnowflakeProps;

/**
 *  snowflake 算法获取id
 * @author Aijm
 * @since 2019/8/24
 */
public class IdUtils extends IdUtil {

    private static final SnowflakeProps sfProps = SpringUtil.getBean(SnowflakeProps.class);

    /**
     * 分布式系统中 获取id
     * @return
     */
    public static long getNextId() {
        Snowflake snowflake = cn.hutool.core.util.IdUtil.createSnowflake(sfProps.getWorkerId(), sfProps.getDataId());
        return snowflake.nextId();
    }
}